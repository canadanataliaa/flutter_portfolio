import 'package:flutter/material.dart';

import 'get_to_know_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            title: 'Get to know me!',
            home: GetToKnowMe(),
        );
    }
}