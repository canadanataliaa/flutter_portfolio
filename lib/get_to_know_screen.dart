import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:google_fonts/google_fonts.dart';

class GetToKnowMe extends StatefulWidget {
  @override
  _GetToKnowMeState createState() => _GetToKnowMeState();
}

class _GetToKnowMeState extends State<GetToKnowMe> with TickerProviderStateMixin {
    late AnimationController controller1, controller2, controller3, controller4, controller5;
    late Animation<double> animation1, animation2, animation3, animation4, animation5;

    @override
    void initState() {
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
        ));

        controller1 = AnimationController(
            duration: Duration(milliseconds: 500),
            vsync: this,
        );

        controller2 = AnimationController(
            duration: Duration(milliseconds: 1000),
            vsync: this,
        );

        controller3 = AnimationController(
            duration: Duration(milliseconds: 1500),
            vsync: this,
        );

        controller4 = AnimationController(
            duration: Duration(milliseconds: 2000),
            vsync: this,
        );

        controller5 = AnimationController(
            duration: Duration(milliseconds: 2500),
            vsync: this,
        );

        animation1 = CurvedAnimation(
            parent: controller1, 
            curve: Curves.easeInQuad
        );

        animation2 = CurvedAnimation(
            parent: controller2, 
            curve: Curves.easeInQuad
        );

        animation3 = CurvedAnimation(
            parent: controller3, 
            curve: Curves.easeIn
        );

        animation4 = CurvedAnimation(
            parent: controller4, 
            curve: Curves.easeInQuad
        );

        animation5 = CurvedAnimation(
            parent: controller5, 
            curve: Curves.easeInQuad
        );

        controller1.forward();
        controller2.forward();
        controller3.forward();
        controller4.forward();
        controller5.forward();

        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        final List<String> techs = [
            'Mobile Application Development', 
            'Flutter', 
            'Dart', 
            'Git', 
            'Programming', 
            'Slack', 
            'Bitbucket', 
            'React native', 
            'Javascript', 
            'NodeJS', 
            'MongoDB', 
            'ExpressJS'
            'Programming', 
            'Slack', 
            'Bitbucket', 
            'React native', 
            'Javascript', 
            'NodeJS', 
            'MongoDB', 
            'ExpressJS'
        ]; 

        Widget appBarSection = FadeTransition(
            opacity: animation1,
            child:
                Container(
                padding: EdgeInsets.fromLTRB(10.0, 46.0, 16.0, 30.0),
                width: MediaQuery.of(context).size.width,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                        IconButton(
                            icon: Icon(Icons.menu, color: Color.fromRGBO(20, 45, 68, 1), size: 27.0),
                            tooltip: 'Logout',
                            onPressed: () => Navigator.pushNamed(context, '/project-list')
                        ),
                        Container(
                            height: 22.8,
                            padding: EdgeInsets.fromLTRB(18.0, 1.5, 18.0, 2.0),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(22.0)),
                                color: Color.fromRGBO(20, 45, 68, .9),
                                border: Border.all(color: Color.fromRGBO(20, 45, 68, .9))
                            ),
                            child: Text('See on mobile view. Web is not yet ready.', style: GoogleFonts.oxygen(fontSize: 12.5, color: Colors.white)),
                        )
                    ]
                )
            )
        );

        Widget avatarCard = Container(
            margin: EdgeInsets.only(top: 88.0, left: 10.0, right: 24.0),
            height: 120.0,
            width: 120.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(60.0)),
                boxShadow: [
                    BoxShadow(
                        blurRadius: 5.0,
                        offset: Offset(0, 5.0),
                        color: Colors.black54
                    ),
                ],
                image: DecorationImage(image: AssetImage('assets/woman.png'), fit: BoxFit.cover)
            )
        );

        Widget infoCard = Container(
            margin: EdgeInsets.only(top: 114.0, right: 30.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Text(
                        'Natalia Canada', 
                        style: GoogleFonts.oxygen(fontWeight: FontWeight.bold, fontSize: 23.5, color: Colors.white)
                    ),
                    SizedBox(height: 2.0),
                    Text(
                        'Flutter Developer Trainee',
                        style: GoogleFonts.oxygen(fontSize: 12.0, color: Colors.white)
                    ),
                    Text(
                        'FFUF Manila Inc.', 
                        style: GoogleFonts.oxygen(fontSize: 12.0, color: Colors.white)
                    ),
                    Container(
                        padding: EdgeInsets.only(top: 10.0),
                        child:
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                            Icon(Icons.place_outlined, color: Colors.white, size: 14.0),
                                            SizedBox(width: 3.0),
                                            Text('Philippines', style: GoogleFonts.oxygen(fontSize: 12.0, color: Colors.white)),
                                        ],
                                    ),
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                            Icon(Icons.email_outlined, color: Colors.white, size: 14.0),
                                            SizedBox(width: 3.0),
                                            Text('canadanataliaa@gmail.com', style: GoogleFonts.oxygen(fontSize: 12.0, color: Colors.white)),
                                        ]
                                    )
                                ],
                            )
                    ),
                ]
            )
        );

        Widget heroSection = FadeTransition(
            opacity: animation2,
            child:
                Container(
                height: 250,
                child: Stack(
                    children: [
                        Positioned(
                            top: 100.0,
                            right: 0.0,
                            left: 12.0,
                            child: Container(
                                height: 140.0,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(70.0),
                                        bottomLeft: Radius.circular(70.0) 
                                    ),
                                    gradient: LinearGradient(
                                        colors: [Color.fromRGBO(171, 43, 29, .8), Color.fromRGBO(20, 45, 68, 1)],
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight
                                    ),
                                ),
                            )
                        ),
                        appBarSection,
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                                avatarCard,
                                infoCard
                            ]
                        )   
                    ]
                )
            )
        );

        Widget techStackSection = Container(
            padding: EdgeInsets.only(top: 14.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Container(
                        padding: EdgeInsets.only(left: 16.0, bottom: 1.0),
                      child: Text(
                          'Technologies',
                          style: GoogleFonts.oxygen(fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(height: 12.0),
                    FadeTransition(
                        opacity: animation3,
                        child:
                            Container(
                                padding: EdgeInsets.only(left: 18.0),
                                height: 30,
                                child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: techs.length,
                                    itemBuilder: (BuildContext context, int index) {
                                        return Container(
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(20.0),
                                                color: Color.fromRGBO(20, 45, 68, .9),
                                                border: Border.all(color: Color.fromRGBO(20, 45, 68, .9))
                                            ),
                                            margin: EdgeInsets.only(right: 15.0),
                                            child: Container(
                                                padding: const EdgeInsets.only(top: 5.0, right: 16.0, left: 16.0),
                                                child: Text(
                                                    techs[index],
                                                    style: GoogleFonts.oxygen(color: Colors.white),
                                                ),
                                            )
                                        );
                                    }
                                )
                            )  
                    )              
                ]
            )
        );

        Widget aboutSection = FadeTransition(
            opacity: animation4,
            child:
                Container(
                    padding: EdgeInsets.only(top: 12.0, left: 10.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Container(
                            padding: EdgeInsets.only(left: 4.0, top: 10.0),
                                child: Text(
                                    'About',
                                    style: GoogleFonts.oxygen(fontSize: 18.0, fontWeight: FontWeight.bold),
                                ),
                            ),
                            SizedBox(height: 12.0),
                            Container(
                                padding: EdgeInsets.only(left: 4.0),
                                child: Column(
                                    children: [
                                        Text(
                                            'Currently a full-time Flutter Developer trainee at FFUF Manila Inc. An enthusiast in pursuing a professional career as a mobile application developer.',
                                            style: GoogleFonts.oxygen(fontSize: 14.8),
                                        )
                                    ]
                                ),
                            )
                        ]
                    )
                )
        );

        Widget createCardView(String imgPath, String title, String date, String position) {
            Widget cardBackground = Positioned(
                top: 20.0,
                left: 20.0,
                child: Material(
                    child: Container(
                        height: 190.0,
                        width: MediaQuery.of(context).size.width*0.85,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20.0),
                            gradient: LinearGradient(
                                colors: [Color.fromRGBO(171, 43, 29, .8), Color.fromRGBO(20, 45, 68, 1)],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight
                            ),
                            boxShadow: [
                                BoxShadow(
                                    blurRadius: 5.0,
                                    offset: Offset(0, 5.0),
                                    color: Colors.black54
                                )
                            ]
                        )
                    )
                )
            );

            Widget cardPhoto = Positioned(
                top: 0,
                left: 30.0,
                child: Card(
                    elevation: 10.0,
                    shadowColor: Colors.grey,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)
                    ),
                    child: Container(
                        height: 220,
                        width: 130,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(color: Color.fromRGBO(171, 43, 29, .3)),
                            image: DecorationImage(
                                fit: BoxFit.contain,
                                image: AssetImage(imgPath)
                            )
                        ),
                    ),
                )
            );

            Widget cardDetails = Positioned(
                top: 60,
                left: 180,
                child: Container(
                    height: 150,
                    width: 160,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            Text(title, style: GoogleFonts.oxygen(fontWeight: FontWeight.bold, color: Colors.white)),
                            Text(position, style: GoogleFonts.oxygen(color: Colors.white)),
                            Divider(color: Colors.white),
                            Text(date, style: GoogleFonts.oxygen(color: Colors.white)),
                        ],
                    ),
                ),
            );

            return Container(
                height: 230,
                child: Stack(
                    children: [
                        cardBackground,
                        cardPhoto,
                        cardDetails
                    ]
                )
            );
        }

        List<Widget> educationCards() {
            List<Widget> cards = [
                createCardView('assets/android.png', 'Android Development', '2018 - 2019', 'Project Design - Capstone'),
                createCardView('assets/admin.png', 'System Administration', '2018 - 2019', 'Course Elective'),
                createCardView('assets/softeng.png', 'Software Engineering', '2018 - 2019', 'Major Course'),
                createCardView('assets/network.png', 'CISCO Networks', '2018 - 2019', 'Networking Course'),
                createCardView('assets/android.png', 'Android Development', '2018 - 2019', 'Project Design - Capstone'),
                createCardView('assets/admin.png', 'System Administration', '2018 - 2019', 'Course Elective'),
                createCardView('assets/softeng.png', 'Software Engineering', '2018 - 2019', 'Major Course'),
                createCardView('assets/network.png', 'CISCO Networks', '2018 - 2019', 'Networking Course'),
            ];

            return cards;
        }

        List<Widget> workCards() {
            List<Widget> cards = [
                createCardView('assets/mobile.png', 'Mobile Application Development', '2021 - Present', 'Flutter Developer Trainee'),
                createCardView('assets/robot.png', 'Robotic Process Automation', '2020 - 2021', 'RPA Developer'),
                createCardView('assets/software.png', 'Software Development', '2020', 'Python, MongoDB, ExpressJS, Angular, NodeJS'),
                createCardView('assets/robot.png', 'Robotic Process Automation', '2019 - 2021', 'RPA Developer'),
                createCardView('assets/mobile.png', 'Mobile Application Development', '2021 - Present', 'Flutter Developer Trainee'),
                createCardView('assets/robot.png', 'Robotic Process Automation', '2020 - 2021', 'RPA Developer'),
                createCardView('assets/software.png', 'Software Development', '2020', 'Python, MongoDB, ExpressJS, Angular, NodeJS'),
                createCardView('assets/robot.png', 'Robotic Process Automation', '2019 - 2021', 'RPA Developer'),
            ];

            return cards;
        }

        Widget workSection = Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    SizedBox(height: 24.0),
                    Container(
                        padding: EdgeInsets.only(left: 14.0),
                    child: Text(
                        'Work Experience',
                        style: GoogleFonts.oxygen(fontSize: 18.0, fontWeight: FontWeight.bold),
                    ),
                    ),
                    SizedBox(height: 14.0),
                    FadeTransition(
                        opacity: animation5,
                        child:
                            Container(
                                height: 255.0,
                                child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: workCards().length,
                                    itemBuilder: (BuildContext context, int index) {
                                        return Container(
                                            margin: EdgeInsets.only(right: 6.0),
                                            height: 290.0,
                                            width: MediaQuery.of(context).size.width*0.9,
                                            child: workCards()[index]
                                        );
                                    }
                                )
                            )
                    )
                ]
            )
        );

        Widget schoolSection = Container(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    SizedBox(height: 20.0),
                    Container(
                        padding: EdgeInsets.only(left: 14.0),
                      child: Text(
                          'Education',
                          style: GoogleFonts.oxygen(fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(height: 14.0),
                    Container(
                        height: 255.0,
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: educationCards().length,
                            itemBuilder: (BuildContext context, int index) {
                                return Container(
                                    margin: EdgeInsets.only(right: 6.0),
                                    height: 290.0,
                                    width: MediaQuery.of(context).size.width*0.9,
                                    child: educationCards()[index]
                                );
                            }
                        )
                    )
                ]
            )
        );
        
        Widget aboutView = SingleChildScrollView(
            child: Column(
                children: [
                    heroSection,
                    techStackSection,
                    aboutSection,
                    workSection,
                    schoolSection,
                ]
            )
        );

        return Scaffold(
            body: Container(
                color: Color.fromRGBO(245, 245, 250, 1),
                child: aboutView
            )
        );
    }
}